import pathlib

import gi.repository.GLib
import dbus
import dbus.service
import dbus.mainloop.glib

import cmus

BUS_NAME = "org.mpris.MediaPlayer2.cmus"
OBJECT_PATH = "/org/mpris/MediaPlayer2"
ROOT_IFACE = "org.mpris.MediaPlayer2"
PLAYER_IFACE = ROOT_IFACE + ".Player"
PLAYLISTS_IFACE = ROOT_IFACE + ".Playlists"

class MprisObject(dbus.service.Object):
    properties = None

    def __init__(self):
        self.properties = {
            ROOT_IFACE: {
                "CanQuit": (False, None),
                "CanRaise": (False, None),
                "HasTrackList": (False, None),
                "Identity": ("cmus", None),
                "SupportedUriSchemes": (dbus.Array([], "s"), None),
                "SupportedMimeTypes": (dbus.Array([], "s"), None)
            },
            PLAYER_IFACE: {
                "PlaybackStatus": (self.get_PlaybackStatus, None),
                "LoopStatus": (self.get_LoopStatus, self.set_LoopStatus),
                "Rate": (1.0, None),
                "Shuffle": (self.get_Shuffle, self.set_Shuffle),
                "Metadata": (self.get_Metadata, None),
                "Volume": (self.get_Volume, self.set_Volume),
                "Position": (self.get_Position, self.set_Position),
                "MinimumRate": (1.0, None),
                "MaximumRate": (1.0, None),
                "CanGoNext": (True, None),
                "CanGoPrevious": (True, None),
                "CanPlay": (True, None),
                "CanPause": (True, None),
                "CanSeek": (True, None),
                "CanControl": (True, None)
            }
        }
        bus_name = dbus.service.BusName(BUS_NAME, dbus.SessionBus())
        super().__init__(bus_name, OBJECT_PATH)

    # Properties methods

    @dbus.service.method(dbus.PROPERTIES_IFACE, "ss", "v")
    def Get(self, interface, prop):
        getter, _ = self.properties[interface][prop]
        return getter() if callable(getter) else getter

    @dbus.service.method(dbus.PROPERTIES_IFACE, "s", "a{sv}")
    def GetAll(self, interface):
        getters = {}
        for key in self.properties[interface]:
            getter, _ = self.properties[interface][key]
            getters[key] = getter() if callable(getter) else getter
        return getters

    @dbus.service.method(dbus.PROPERTIES_IFACE, "ssv")
    def Set(self, interface, prop, value):
        _, setter = self.properties[interface][prop]
        if setter is not None:
            setter(value)
            self.PropertiesChanged(interface, {prop: self.Get(interface, prop)}, [])
        else:
            # Yes, this is mildly sketchy
            self.PropertiesChanged(interface, {prop: self.Get(interface, prop)}, [])

    @dbus.service.signal(dbus.PROPERTIES_IFACE, "sa{sv}as")
    def PropertiesChanged(self, interface, changed_properties, invalidated_properties):
        pass

    # Root iface methods

    @dbus.service.method(ROOT_IFACE)
    def Raise(self):
        pass

    @dbus.service.method(ROOT_IFACE)
    def Quit(self):
        pass

    # Player iface methods

    @dbus.service.method(PLAYER_IFACE)
    def Next(self):
        cmus.CmusRemote.next()

    @dbus.service.method(PLAYER_IFACE)
    def Previous(self):
        cmus.CmusRemote.prev()

    @dbus.service.method(PLAYER_IFACE)
    def Pause(self):
        cmus.CmusRemote.pause()

    @dbus.service.method(PLAYER_IFACE)
    def PlayPause(self):
        cmus.CmusRemote.pause()

    @dbus.service.method(PLAYER_IFACE)
    def Stop(self):
        cmus.CmusRemote.stop()

    @dbus.service.method(PLAYER_IFACE)
    def Play(self):
        cmus.CmusRemote.play()

    @dbus.service.method(PLAYER_IFACE)
    def Seek(self, offset):
        cmus.CmusRemote.seek(int(offset / 1000000))
        np: cmus.MusicInfo = cmus.CmusRemote.get_playing()
        self.Seeked(self.get_Position())

    @dbus.service.method(PLAYER_IFACE)
    def SetPosition(self, trackid, position):
        np: cmus.MusicInfo = cmus.CmusRemote.get_playing()
        print(offset / 1000000)
        cmus.CmusRemote.seek(int(offset / 1000000), True)
        self.Seeked(self.get_Position())

    # Player iface signals

    @dbus.service.signal(PLAYER_IFACE, "x")
    def Seeked(self, position):
        pass

    # Getters

    def get_PlaybackStatus(self):
        s = cmus.CmusRemote.get_playing().status
        return s.title()

    def get_LoopStatus(self):
        return False

    def get_Shuffle(self):
        return False

    def get_Metadata(self):
        np: cmus.MusicInfo = cmus.CmusRemote.get_playing()
        return dbus.Dictionary({
            "mpris:trackid": pathlib.Path(np.filename).as_uri(),
            "mpris:length": dbus.Int64(np.duration * 1000000),
            "xesam:title": np.title,
            "xesam:artist": np.artist,
            "xesam:album": np.album,
            "xesam:artUrl": pathlib.Path(np.cover).as_uri()
        }, "sv")

    def get_Volume(self):
        return cmus.CmusRemote.get_playing().volume / 100

    def get_Position(self):
        return cmus.CmusRemote.get_playing().position * 1000000

    # Setters

    def set_Volume(self, val):
        cmus.CmusRemote.volume(int(val * 100))

    def set_LoopStatus(self, val):
        pass

    def set_Shuffle(self, val):
        pass

    def set_Position(self, val):
        self.Seeked(self.get_Position())

if __name__ == "__main__":
    dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
    obj = MprisObject()

    gi.repository.GLib.MainLoop().run()
