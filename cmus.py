"""Python bindings for cmus-remote"""

import hashlib
import os
import re
import subprocess
import sys
import tempfile

from typing import Any, Dict, List

CACHE_DIR = os.path.expanduser("~/.cache/cmus")

if not os.path.exists(CACHE_DIR):
    os.makedirs(CACHE_DIR)

# Sadly, we don't know the type of a compiled regex
REGEXES: Dict[str, Any] = {
    "STATUS_REGEX": r'^status\ (.+)$',
    "FILENAME_REGEX": r'^file\ (.+)$',
    "DURATION_REGEX": r'^duration\ (\d+)$',
    "POSITION_REGEX": r'^position\ (\d+)$',
    "TITLE_REGEX": r'^tag title\ (.+)$',
    "ARTIST_REGEX": r'^tag artist\ (.+)$',
    "ALBUM_REGEX": r'^tag album\ (.+)$',
    "TRACK_REGEX": r'^tag tracknumber\ (\d+)$',
    "SHUFFLE_REGEX": r'^set shuffle\ (true|false)$',
    "VOLUME_REGEX": r'^set vol_left (\d+)$'
}

# Compile all regexes
for rname in REGEXES:
    REGEXES[rname] = re.compile(REGEXES[rname], re.M)


def run_devnull(cmd: List[str]) -> None:
    subprocess.check_call(cmd, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)


def str_md5(s: str) -> str:
    return hashlib.md5(s.encode()).hexdigest()

class MusicInfo:
    """Main class"""
    status: str = str()
    filename: str = str()
    duration: int = int()
    cover: str = str()
    position: int = int()
    title: str = str()
    artist: str = str()
    album: str = str()
    track: int = int()
    shuffle: bool = bool()
    volume: int = int()

    def update_info(self) -> None:
        cmd_r = subprocess.check_output(["cmus-remote", "-Q"]).decode()

        for field_r in REGEXES:
            name = "_".join(field_r.lower().split("_")[:-1])
            f_type = type(getattr(self, name))
            if f_type == bool:
                f_type = lambda x: x == "true"
            try:
                setattr(self, name, f_type(REGEXES[field_r].findall(cmd_r)[0]))
            except IndexError as e:
                setattr(self, name, "Unknown")

        self.cover = os.path.join(CACHE_DIR, str_md5(self.filename)) + ".png"

    def update_cover(self) -> None:
        """Updates COVER_FILE"""
        run_devnull(["ffmpeg", "-y", "-i", self.filename, self.cover])

class CmusRemote:
    @staticmethod
    def _run_cmd(cmd: str) -> None:
        run_devnull(["cmus-remote", "-C", cmd])

    @staticmethod
    def get_playing() -> MusicInfo:
        m = MusicInfo()
        m.update_info()
        return m

    @staticmethod
    def volume(left: int, right: int = None) -> None:
        if right is None:
            right = left
        CmusRemote._run_cmd(f"vol {left}% {right}%")

    @staticmethod
    def prev() -> None:
        CmusRemote._run_cmd("player-prev")

    @staticmethod
    def play() -> None:
        CmusRemote._run_cmd("player-play")

    @staticmethod
    def pause() -> None:
        CmusRemote._run_cmd("player-pause")

    @staticmethod
    def stop() -> None:
        CmusRemote._run_cmd("player-stop")

    @staticmethod
    def next() -> None:
        CmusRemote._run_cmd("player-next")

    @staticmethod
    def stop() -> None:
        CmusRemote._run_cmd("player-stop")

    @staticmethod
    def seek(seconds: int, absolute: bool = False) -> None:
        if not absolute:
            time_s = str(seconds)
            if time_s[0] != "-":
                time_s = "+" + time_s
            CmusRemote._run_cmd(f"seek {time_s}")
        else:
            CmusRemote._run_cmd(f"seek {seconds}")
